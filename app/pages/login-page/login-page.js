import { CellsPage } from '@cells/cells-page';
import { BbvaCoreIntlMixin } from '@bbva-web-components/bbva-core-intl-mixin';
import { html, css } from 'lit-element';

import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';

import { cleanUp } from '../../elements/movements-dm/movements-dm';

import '@capa-cells/pokemon-dm/pokemon-dm.js';
import '@capa-cells/pokemon-ui/pokemon-ui.js';

//"@capa-cells/pokemon-dm": "git+ssh://git@bitbucket.org:fredlajogarcia/pokemon-dm.git"

/* eslint-disable new-cap */
class LoginPage extends BbvaCoreIntlMixin(CellsPage) {
  static get is() {
    return 'login-page';
  }


  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="POKEMON - APP">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <pokemon-ui
            id="PokemonUi"
            @obtenerPokemon="${this.getList}"
            @seleccionPokemon="${this.getDetail}"
          ></pokemon-ui>
          <pokemon-dm
          id="PokemonDm"
            @getPokemons-success="${this.listaok}"
            @getPokemon-success="${this.seleccionPoke}"
            @getPokemons-error="${this.viewError}"

          ></pokemon-dm>
          
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  getDetail(evt) {

    //console.log('PokemonDetail....',evt);
    let PokemonDm = this.shadowRoot.querySelector('#PokemonDm');
    PokemonDm.pokemonID = evt.detail;
    PokemonDm.getPokemonDetail();
  }

  getList(evt) {

    //console.log('PokemonList....',evt);
    let PokemonDm = this.shadowRoot.querySelector('#PokemonDm');
    PokemonDm.limite = evt.detail.limite;
    PokemonDm.offset = evt.detail.offset;
    PokemonDm.getListPokemons(evt.detail);
  }


  listaok(evt) {
    let PokemonDm = this.shadowRoot.querySelector('#PokemonDm');
    let pokemonUi = this.shadowRoot.querySelector('#PokemonUi');
    let list = PokemonDm.lista;
    pokemonUi.lista2 = list;
    pokemonUi.llenarContenedor();
  }


  seleccionPoke() {
    let PokemonDm = this.shadowRoot.querySelector('#PokemonDm');
    let pokemonUi = this.shadowRoot.querySelector('#PokemonUi');

    //console.log("foto1 ",PokemonDm.pokemon.sprites.front_default);
    pokemonUi.pokemon.sprite = PokemonDm.pokemon.sprites.front_default;
    pokemonUi.pokemon.spriteback = PokemonDm.pokemon.sprites.back_default;
    pokemonUi.pokemon.shiny = PokemonDm.pokemon.sprites.front_shiny;
    pokemonUi.pokemon.shinyback = PokemonDm.pokemon.sprites.back_shiny;
    pokemonUi.pokemon.spritefemale = PokemonDm.pokemon.sprites.front_default;
    pokemonUi.pokemon.spritebackfemale = PokemonDm.pokemon.sprites.back_default;
    pokemonUi.pokemon.shinyfemale = PokemonDm.pokemon.sprites.front_shiny;
    pokemonUi.pokemon.shinybackfemale = PokemonDm.pokemon.sprites.back_shiny;

    //pokemonUi.pokemon.artwork=PokemonDm.pokemon.sprites.other.home.front_default;
    pokemonUi.pokemon.artwork = PokemonDm.pokemon.sprites.other['official-artwork'].front_default;
    pokemonUi.pokemon.stats = PokemonDm.pokemon.stats;
    pokemonUi.mostrar();
    pokemonUi.requestUpdate();

    //console.log("pokemon...",PokemonDm.pokemon.sprites.other['official-artwork'].front_default);
  }

  viewError(evt) {
    console.log('LoginPage.viewError', evt);
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);